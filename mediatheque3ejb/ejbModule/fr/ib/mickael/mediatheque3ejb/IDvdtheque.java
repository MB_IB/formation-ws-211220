
package fr.ib.mickael.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.Remote;

/**
 * @author mic
 *
 */
@Remote
public interface IDvdtheque {
	public String getInfos();
	public boolean ouvertA(LocalTime t);
	public LocalTime getDerniereInterrogation();
}
