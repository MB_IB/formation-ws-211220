package fr.ib.mickael.mediatheque3ejb;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * @author mic
 *
 */
@Stateless(name="DvdDAO", description = "Stockage JPA des DVD")
public class DvdDAO implements IDvdDAO {
	
	private EntityManager em;
	
	public int getNombre() {
		// count(*) renvoie un Long, converti en int retourné par la fonction 
		return em.createQuery("select count(*) from Dvd", Long.class).
				getSingleResult().intValue();
	}

	@Override
	public void ajouter(Dvd dvd) {
		em.persist(dvd);
	}

	@Override
	public Dvd lire(int id) {
		return em.find(Dvd.class, id);
	}

	@Override
	public List<Dvd> lireTous() {
		return em.createQuery("from Dvd order by annee", Dvd.class).getResultList();
	}

	@PersistenceContext(unitName = "DvdPU")
	public void setEm(EntityManager em) {
		this.em = em;
	}	
}
