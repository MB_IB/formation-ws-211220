
package fr.ib.mickael.mediatheque3ejb;

import java.time.LocalTime;

import javax.ejb.EJB;
import javax.ejb.Stateful;
// import javax.ejb.Stateless;

/**
 * @author mic
 *
 */
// NON : @Stateless, car il y a un attribut
@Stateful(name="Dvds", description="Opération pour la DVDThèque")
public class Dvdtheque implements IDvdtheque {
	private LocalTime derniereInterrogation;
	private IDvdDAO dvdDAO;
	
	public String getInfos() {
		return "Nouvelle DVDthèque, ouverte de 10h à 18h.\n"+
				"Il y a "+dvdDAO.getNombre()+" DVD.";		
	}
	
	public boolean ouvertA(LocalTime t) {
		derniereInterrogation = t;
		return t.isAfter(LocalTime.of(10, 0)) &&
				t.isBefore(LocalTime.of(18, 0));
	}
	
	public LocalTime getDerniereInterrogation() {
		return derniereInterrogation;
	}

	//@EJB(lookup="ejb:/Mediatheque3Ejb/DvdDAO!fr.ib.mickael.mediatheque3ejb.IDvdDAO")
	@EJB(name = "DvdDAO")
	public void setDvdDAO(IDvdDAO dvdDAO) {
		this.dvdDAO = dvdDAO;
	}
}
