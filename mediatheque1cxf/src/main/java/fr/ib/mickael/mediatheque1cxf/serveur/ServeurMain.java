package fr.ib.mickael.mediatheque1cxf.serveur;

import javax.xml.ws.Endpoint;

/**
 * @author mic
 *
 */
public class ServeurMain {

	public static void main(String[] args) {
		System.out.println("Démarrage du serveur");
		Endpoint epLivres = Endpoint.publish(
			"http://localhost:9001/livres",
			new LivresService());		
		// ne pas garder, car dépublie le service dès le début // epLivres.stop();
	}

}
