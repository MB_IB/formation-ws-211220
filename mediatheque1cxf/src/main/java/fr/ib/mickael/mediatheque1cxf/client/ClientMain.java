package fr.ib.mickael.mediatheque1cxf.client;

import org.apache.cxf.interceptor.LoggingInInterceptor;
import org.apache.cxf.interceptor.LoggingOutInterceptor;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;

import fr.ib.mickael.mediatheque1cxf.serveur.ILivresService;
import fr.ib.mickael.mediatheque1cxf.serveur.Livre;

/**
 * @author mic
 *
 */
public class ClientMain {
	public static void main(String[] args) {
		System.out.println("Client livres :");
		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();		
		factory.setAddress("http://localhost:9001/livres");
		factory.setServiceClass(ILivresService.class);
		factory.getOutInterceptors().add(new LoggingOutInterceptor());
		factory.getInInterceptors().add(new LoggingInInterceptor());		
		ILivresService livresService = factory.create(ILivresService.class);
		System.out.println(livresService.getInfos());
		System.out.println("Livre 4 empruntable : "+livresService.estEmpruntable(4));
		try {
			System.out.println("Livre -3 empruntable : "+livresService.estEmpruntable(-3));
		} catch(Exception ex) {
			System.out.println("Exception : "+ex.getMessage());
		}
		System.out.println("Retour du livre 4 : "+livresService.getRetour(4));
		System.out.println("Livre du mois : "+livresService.getLivreDuMois());
		Livre[] la = livresService.getLivresDeLannee(); 
		System.out.println("Livre de mars : "+la[2].getTitre());
	}
}
