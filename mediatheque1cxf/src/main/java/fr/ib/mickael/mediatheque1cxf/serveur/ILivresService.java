package fr.ib.mickael.mediatheque1cxf.serveur;

import java.util.Date;

import javax.jws.WebService;

/**
 * Procédures accessibles au client
 * @author mic
 */
@WebService
public interface ILivresService {
	public String getInfos();
	public boolean estEmpruntable(int id);
	public Date getRetour(int id);
	public Livre getLivreDuMois();
	public Livre[] getLivresDeLannee();
}
