package fr.ib.mickael.mediatheque1cxf.serveur;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.jws.WebService;

/**
 * Code des procédures du service
 * @author mic
 */
@WebService
public class LivresService implements ILivresService {
	public String getInfos() {
		return "L'entrée de la bibliothèque se trouve avenue Victor Hugo";
	}

	public boolean estEmpruntable(int id) {
		if(id<1)
			throw new IllegalArgumentException("id doit être 1 ou plus");
		return false;
	}
	
	public Date getRetour(int id) {
		// renvoyer toujours : aujourd'hui + 10 jours
		//return LocalDate.now().plusDays(10);
		LocalDate d = LocalDate.now();
		d = d.plusDays(10);
		return Date.from(d.atStartOfDay(ZoneId.systemDefault()).toInstant());
	}

	public Livre getLivreDuMois() {
		Livre l = new Livre("La plus secrete...", "M. M. Sarr", 2021);
		DataSource dataSource = new FileDataSource("la-plus-secrete.jpg");
		DataHandler dataHandler = new DataHandler(dataSource);
		l.setImage(dataHandler);
		return l;
	}

	public Livre[] getLivresDeLannee() {
		Livre[] livres = new Livre[12];
		for(int i =0 ; i<12 ; i++) {
			livres[i] = getLivreDuMois();		
		}
		return livres;
	}	
}
