
package fr.ib.mickael.mediatheque2spring.clients;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import fr.ib.mickael.mediatheque2spring.Cd;

/**
 * @author mic
 */
public class ClientSpringMain {

	public static void main(String[] args) {
		try {
			System.out.println("Client Java Spring");
			RestTemplate rt = new RestTemplate();
			String health = rt.getForObject("http://localhost:9002/health", 
					String.class);
			System.out.println("Health : "+health);
			Integer nbCd = rt.getForObject("http://localhost:9002/cd/nombre", 
					Integer.class);
			System.out.println("Il y a "+nbCd+" cd à la médiathèque");
			Cd cd1 = new Cd("Abbey Road", "The Beatles", 1966);
			rt.postForObject("http://localhost:9002/cd", cd1, Void.class);	
			
			// Ne fonctionne pas (car List est générique) :
			// List<Cd> cds = rt.getForObject("http://localhost:9002/cd",
			//		List<Cd>.class);
			// Solution 1 :
			/*
			ParameterizedTypeReference<List<Cd>> ref = 
					new ParameterizedTypeReference<List<Cd>>() {};
			ResponseEntity<List<Cd>> cdsEntity = 
					rt.exchange("http://localhost:9002/cd",	
					HttpMethod.GET, null, ref);
			List<Cd> cds = cdsEntity.getBody();
			*/
			// Solution 2 : 
			Cd[] cds = rt.getForObject("http://localhost:9002/cd", Cd[].class);
			System.out.println("Tous les CD : ");
			for(Cd cd:cds) {
				System.out.println(" - "+cd);
			}
			
			// paramètre de requete : "http://localhost:9002/cd?no=0"
			Cd cd0 = rt.getForObject("http://localhost:9002/cd/0", Cd.class);
			System.out.println("Premier CD : "+cd0);
			
			rt.put("http://localhost:9002/cd/0/titre", "Help!", String.class);
			
		} catch(Exception ex) {
			System.err.println("Erreur : "+ex);
		}
	}

}
