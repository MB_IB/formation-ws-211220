
package fr.ib.mickael.mediatheque2spring;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mic
 *
 */
@RestController
public class HealthController {
	@RequestMapping("/health")
	public String getHealth() {
		return "OK";
	}
}
