
package fr.ib.mickael.mediatheque4web;

import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;


/**
 * @author mic
 *
 */
public class DvdsMessageMain {

	public static void main(String[] args) {
		System.out.println("Envoi de message");
		try {
			Context context = new InitialContext();
			// Usine à connection vers le système JMS
			QueueConnectionFactory qcf = (QueueConnectionFactory)
					context.lookup("jms/RemoteConnectionFactory");
			// Connexion vers le serveur JMS
			QueueConnection qc = qcf.createQueueConnection();
			// Session : suite d'ordres JMS
			QueueSession qs = qc.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
			// File de message/boîte aux lettres
			// dans standalone.xml : 
			// <jms-queue name="dvd" entries="java:jboss/exported/jms/queue/dvd" durable="true"/>
			Queue q = (Queue)context.lookup("jms/queue/dvd");
			// Outil qui envoi le message
			QueueSender qse = qs.createSender(q);
			TextMessage message1 = qs.createTextMessage("Nouveau DVD !");
			qse.send(message1);
			qse.close();
			qs.close();
			qc.close();	
			
			// TOODO envoi vers un topicZ			
			
			context.close();			
		} catch(Exception ex) {
			System.err.println("Erreur : "+ex);
		}

	}

}
