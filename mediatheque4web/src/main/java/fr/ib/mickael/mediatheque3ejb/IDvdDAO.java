package fr.ib.mickael.mediatheque3ejb;

import java.util.List;

import javax.ejb.Remote;

/**
 * @author mic
 *
 */
@Remote
public interface IDvdDAO {
	public int getNombre();
	public void ajouter(Dvd dvd);
	public Dvd lire(int id);
	public List<Dvd> lireTous();
}
